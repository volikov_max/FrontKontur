//Task_1
const convertCelsiusToFahrenheit = (degrees) => (degrees * 9 / 5) + 32;

convertCelsiusToFahrenheit(36);
/////////////////////////////////////////////////////////////////////////


//Task_2
function convertStringToNumber(str) {
  const res = Number(str);
  if (typeof res === 'number' && !Number.isNaN(res)) {
    return res;
  } else {
    return false;
  }
}

convertStringToNumber('123');
/////////////////////////////////////////////////////////////////////////


//Task_3
function getNaN () {
    return +'abc';
}

getNaN();
/////////////////////////////////////////////////////////////////////////


//Task_4
function createGratitude(name = 'Аноним', rating = 0) {
  if (rating < 1 || rating > 5) {
    rating = 0;
  }
  return `${name} оценил нас на ${rating} из 5. Спасибо, ${name}!`;
}

createGratitude('Maximka', 5);
////////////////////////////////////////////////////////////////////////


//Task_5
function checkA1 (a) {
  if (a === 0) {
    return 'Всё плохо.';
  }
  return a;
}

checkA1(0);


function checkA2 (a) {
  return a === 0 ? 'Всё плохо.' : a;
}

checkA2(0);


function checkA3 (a) {
  return a || 'Всё плохо.';
}

checkA3(0);


//Task_6
function squaresSum(min, max) {
  let square;
  let res = 0;
  while (min != (max + 1)) {
    square = min * min;
    res = res + square;
    min++;
  }
  return res;
}

squaresSum(1, 3);



const fn = (first, second) =>
 new Array(second - first + 1)
 .fill(first)
 .reduce((acc, val, ind) => acc + (val + ind)**2, 0)

 fn(1, 3)
